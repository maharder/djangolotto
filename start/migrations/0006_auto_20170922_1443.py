# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-22 12:43
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('start', '0005_auto_20170922_1439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='benutzer',
            name='datum',
            field=models.DateTimeField(default='2017-09-22 14:43:27.375991'),
        ),
        migrations.AlterField(
            model_name='einstellungen',
            name='datum',
            field=models.DateTimeField(default=datetime.datetime(2017, 9, 22, 14, 43, 27, 376991)),
        ),
        migrations.AlterField(
            model_name='einstellungen',
            name='deanzahl',
            field=models.PositiveSmallIntegerField(blank=True, default=1, null=True),
        ),
        migrations.AlterField(
            model_name='einstellungen',
            name='euanzahl',
            field=models.PositiveSmallIntegerField(blank=True, default=1, null=True),
        ),
    ]
