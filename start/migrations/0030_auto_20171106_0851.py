# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-06 07:51
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('start', '0029_auto_20171106_0838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='einstellungen',
            name='datum',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 6, 8, 51, 12, 158696)),
        ),
    ]
