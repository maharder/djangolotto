# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-20 14:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('start', '0002_auto_20170920_0933'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='benutzer',
            name='id',
        ),
        migrations.AddField(
            model_name='benutzer',
            name='datum',
            field=models.DateTimeField(default='2017-09-20 16:11:02.746048'),
        ),
        migrations.AlterField(
            model_name='benutzer',
            name='name',
            field=models.CharField(max_length=264, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='einstellungen',
            name='datum',
            field=models.DateTimeField(default='2017-09-20 16:11:02.746048'),
        ),
        migrations.AlterField(
            model_name='einstellungen',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
