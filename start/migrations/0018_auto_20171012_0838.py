# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-12 06:38
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('start', '0017_auto_20171011_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='einstellungen',
            name='datum',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 12, 8, 38, 42, 423783)),
        ),
    ]
