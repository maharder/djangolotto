# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-20 07:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('start', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='einstellungen',
            name='datum',
            field=models.DateTimeField(default='2017-09-20 09:33:59.905048'),
        ),
    ]
