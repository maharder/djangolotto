from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect, response
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.models import User
from .models import einstellungen
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
import datetime
# Create your views here.
def index(request):
    context = {
        'title': "Startseite der Lotterie",
        'description': "Die Seite zum Vergleichen und Spielen!"
    }
    if request.user.is_authenticated():
        logedin = request.COOKIES.get('user_login')
        if not logedin:
            return HttpResponseRedirect("/signout/")
        else:
            return render(request,"lotto/start.html",context)
    else:
        return render(request,"lotto/start.html",context)

def signin(request):
    context = {
        'title': "Die Loginseite",
        'description': "Benutzerloginbereich.",
        'nbar': "login",
    }
    return render(request,"lotto/login.html",context)

@login_required
def signout(request):
    context = {
        'title': "",
        'description': "",
        'nbar': "logout",
    }
    site = render(request, 'lotto/logged.html')
    site.delete_cookie(key='user_login')
    logout(request)
    return site

def signup(request):
    if request.user.is_authenticated():
        fehler ="Du bist ja schon eingeloggt! Falls dies ein Fehler ist - leere dein Browsercache und -cookies!"
        context = {
            'title': "Benutzerregistrierung",
            'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
            'fehler': fehler,
        }
        return render(request, "lotto/fehler.html", context)
    else:
        context = {
            'title': "Benutzerregistrierung",
            'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
            'nbar': "register",
        }
        return render(request,"lotto/register.html",context)

def doReg(request):
    fehler = ""
    context = {
        'title': "Benutzerregistrierung",
        'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
        'fehler': fehler,
    }
    if request.POST:
        username = request.POST['name']
        userpass = request.POST['pw1']
        usermail = request.POST['email']
        if userpass != request.POST['pw2']:
            fehler ="Die Passwörter stimmen nich überein!"
            context = {
                'title': "Benutzerregistrierung",
                'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
                'fehler': fehler,
            }
            return render(request, "lotto/fehler.html", context)
        elif userpass == "" or userpass == None:
            fehler = "Passwort darf nicht leer sein!"
            context = {
                'title': "Benutzerregistrierung",
                'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
                'fehler': fehler,
            }
            return render(request, "lotto/fehler.html", context)
        elif usermail == "" or usermail == None:
            fehler = "E-Mail darf nicht leer sein!"
            context = {
                'title': "Benutzerregistrierung",
                'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
                'fehler': fehler,
            }
            return render(request, "lotto/fehler.html", context)
        elif username == "" or username == None:
            fehler = "Benutzername darf nicht leer sein!"
            context = {
                'title': "Benutzerregistrierung",
                'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
                'fehler': fehler,
            }
            return render(request, "lotto/fehler.html", context)
        else:
            new_user = User.objects.create_user(username, usermail, userpass)
            return HttpResponseRedirect('/login/')
    else:
        fehler ="Die Registrierung ist fehlgeschlagen!"
        context = {
            'title': "Benutzerregistrierung",
            'description': "Erstelle ein Profil und speichere deine Glückszahlen!.",
           'fehler': fehler,
        }
        return render(request, "lotto/fehler.html", context)

def dologin(request):
    username = request.POST['name']
    userpass = request.POST['pw1']
    user = authenticate(username=username,password=userpass)
    
    if user is not None:
        if user.is_active:
            site = render(request, 'lotto/logged.html')
            site.set_cookie(key='user_login', value=username)
            login(request, user)
            return site
        else: 
            fehler ="Anscheinend wurde Ihr Acoount gesperrt! Wenden Sie sich and den Admin!"
            context = {
                'title': "Einloggen fehlgeschlagen!",
                'description': "Irgendwas lief schief beim Einloggen!.",
               'fehler': fehler,
            }
            return render(request, "lotto/fehler.html", context)
    else:
        fehler ="Passwort oder Benutzername wurden falschen eingegeben!"
        context = {
            'title': "Einloggen fehlgeschlagen!",
            'description': "Irgendwas lief schief beim Einloggen!.",
           'fehler': fehler,
        }
        return render(request, "lotto/fehler.html", context)

def mailcheck(request):
    if request.GET:
        usermail = request.GET['email']
        mails = User.objects.filter(email=usermail)
        if mails.count() <= 0:
            return HttpResponse("ok", content_type="text/html")
        elif mails.email == usermail:
            return HttpResponse("same", content_type="text/html")
        else:
            return HttpResponse("no", content_type="text/html")
    else:
        pass

def namecheck(request):
    if request.GET:
        username = request.GET['name']
        names = User.objects.filter(username=username)
        if names.count() <= 0:
            return HttpResponse("ok", content_type="text/html")
        else:
            return HttpResponse("no", content_type="text/html")
    else:
        pass

@login_required
def settings(request, uname):
    if request.user.is_authenticated():
        user = User.objects.get(username=uname)
        username = user.username
        logedin = request.COOKIES.get('user_login')
        if username == logedin:
            usermail = user.email
            trusted = True
        else:
            usermail = "********"
            trusted = False
        userreg = user.date_joined
        userlast = user.last_login
        einstellungen.objects.get_or_create(uname=username)
        ben = einstellungen.objects.get(uname=username)
        euro = ben.euanzahl
        detl = ben.deanzahl
        ziehung = ben.ziehung
        
        context = {
            'title': "Benutzereinstellungen von "+username,
            'description': "Interne Seite jedes Benutzers",
            'login': username,
            'email': usermail,
            'regdate': userreg,
            'lastvisit': userlast,
            'trust': trusted,
            'de': detl,
            'ziehung': ziehung,
            'eu': euro,
            'nbar': "profil",
        }
        
        return render(request,"lotto/settings.html",context)
    else:
        return HttpResponseRedirect("/login/")

@login_required
def doedit(request):
    if request.user.is_authenticated():
        if request.POST:
            user = request.POST['user_login']
            de = request.POST['de']
            eu = request.POST['eu']
            ziehung = request.POST['ziehung']
            mail = request.POST['email']
            upass = request.POST['pw']
            
            user_set = einstellungen.objects.get(uname=user)
            user_set.deanzahl = de
            user_set.euanzahl = eu
            user_set.ziehung = ziehung
            user_set.datum = datetime.datetime.now()
            user_set.save()
            
            user_prof = User.objects.get(username=user)
            user_prof.email = mail
            if len(upass) > 0:
                user_prof.set_password(upass)
            else: 
                pass
            user_prof.save()
            
            return HttpResponseRedirect('/user/'+user+"/")
            
        else:
            pass