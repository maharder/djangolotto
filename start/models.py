from django.db import models
from django.contrib.auth.models import User
import datetime
    
class einstellungen(models.Model):
    uname = models.CharField(primary_key=True,max_length=150,unique=True)
    deanzahl = models.PositiveSmallIntegerField(default=1)
    ziehung = models.PositiveSmallIntegerField(default=1)
    euanzahl = models.PositiveSmallIntegerField(default=1)
    datum = models.DateTimeField(default=datetime.datetime.now())
    
    def __str__(self):
        out = str(self.uname) + " (Letze Aktualisierung am: "+str(self.datum)+")"
        return out