from django.apps import AppConfig


class EuropaConfig(AppConfig):
    name = 'europa'
