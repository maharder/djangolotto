from django.db import models
import datetime

class Ziehung(models.Model):
    datum = models.DateField(default=str(datetime.datetime.now())[:10],primary_key=True)
    zahl1 = models.IntegerField()
    zahl2 = models.IntegerField()
    zahl3 = models.IntegerField()
    zahl4 = models.IntegerField()
    zahl5 = models.IntegerField()
    zahl6 = models.IntegerField()
    zahls = models.IntegerField()
    gk1 = models.FloatField()
    gk2 = models.FloatField()
    gk3 = models.FloatField()
    gk4 = models.FloatField()
    gk5 = models.FloatField()
    gk6 = models.FloatField()
    gk7 = models.FloatField()
    gk8 = models.FloatField()
    gk9 = models.FloatField(default=5)
    
    def __str__(self):
        return str(self.datum)