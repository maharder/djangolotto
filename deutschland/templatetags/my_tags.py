import re
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()

@register.filter(name="cut")
def cut(val,arg):
    """
    Entfernt alle "arg" aus "val"
    """
    return val.replace(arg,'')

@register.filter('startswith')
def startswith(text, starts):
    if isinstance(text, basestring):
        return text.startswith(starts)
    return False