# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-09 09:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deutschland', '0014_auto_20171006_0841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ziehung',
            name='datum',
            field=models.DateField(default='2017-10-09', primary_key=True, serialize=False),
        ),
    ]
