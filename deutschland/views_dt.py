from django.shortcuts import render
from deutschland.models import Ziehung
from django.http import HttpResponse
from start.models import einstellungen
from django.views.generic import (View, TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView)
from bs4 import BeautifulSoup
import requests, datetime, calendar
from calendar import monthrange
from django.http.response import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist

class zahltag(DetailView):
    model = Ziehung

    def get_context_data(self, **kwargs):
        context = super(zahltag, self).get_context_data(**kwargs)
        context['title'] = "Gewinnziehung am: "+str(Ziehung.datum)
        context['description'] = "Alle Eckdaten der Ziehung am "+str(Ziehung.datum)
        context['nbar'] = "archiv"
        return context
    
class archiv(ListView):
    model = Ziehung
    context_object_name = 'data_list'

    def get_context_data(self, **kwargs):
        context = super(archiv, self).get_context_data(**kwargs)
        context['title'] = "Archiv der Zieuhngen in 6 aus 49 Lotto"
        context['description'] = "Auflistung aller Zahlen und Daten auf einer Seite"
        context['nbar'] = "archiv"
        return context

def index(request):
    context = {
        'title': "Die Lotterie 6 aus 49",
        'description': "Das deutsche Lotto!",
        'nbar': "home",
    }
    return render(request,"lotto/index.html",context)

def generator(request):
    if request.user.is_authenticated():
        nutzer = einstellungen.objects.get_or_create(uname=request.user.username)
        ben = einstellungen.objects.get(uname=request.user.username)
        de = ben.deanzahl
        ziehung = ben.ziehung
    else:
        de = 1
        ziehung = 3
        
    context = {
        'title': "Die Lotterie 6 aus 49: Generator",
        'description': "Erstelle selbst, konfiguriere automatisch oder vergleiche deine Zahlen mit den Gewinnzahlen!",
        'nbar': "generator",
        'anzahl': de,
        'ziehung': ziehung,
    }
    return render(request,"lotto/generator.html",context)

def parsen(request):
    if request.GET:
        url = requests.get('http://winnersystem.org/archiv/')
        soup = BeautifulSoup(url.content, "html.parser")
        
        for reihe in soup.find_all("tr"):
            list_my = []
            for spalte in reihe.find_all("td"):
                tip = spalte.get_text()
                list_my.append(tip.replace("-", "0"))
            
            if len(list_my) > 0:
                if list_my[8] != "0":
                    datep = list_my[0].split(".")
                    list_my[0] = datep[2]+"-"+datep[1]+"-"+datep[0]
                    
                    zih = Ziehung.objects.get_or_create(datum=list_my[0], zahl1=list_my[1], zahl2=list_my[2], zahl3=list_my[3], zahl4=list_my[4], zahl5=list_my[5], zahl6=list_my[6], zahls=list_my[7], gk1=list_my[8], gk2=list_my[9], gk3=list_my[10], gk4=list_my[11], gk5=list_my[12], gk6=list_my[13], gk7=list_my[14], gk8=list_my[15])
        
        return HttpResponse("ok", content_type="text/html")
    else:
        return HttpResponse("no", content_type="text/html")

def getzahlen(request):
    if request.GET:
        if len(request.GET['datum']) > 0:
            spange = request.GET['range']
            months = [31,28,31,30,31,30,31,31,30,31,30,31]
            datum = request.GET['datum'].split(" - ")
            new_datum = []
            if len(datum)>1:
                new = datum[1].split(".")
                last = datum[0].split(".")
                new_year = int(new[2])-int(last[2])
                years = []
                month = []
                if new_year != 0:
                    new_year = new_year+1
                    for y in range(new_year):
                        ny = int(new[2])-int(y)
                        years.append(ny)
                else:
                    years.append(new[2])
                    
                new_month = int(new[1])-int(last[1])
                if new_month != 0:
                    new_month = new_month+1
                    for m in range(new_month):
                        nm = int(new[1])-int(m)
                        month.append(nm)
                else:
                    month.append(new[1])
                    
                datetimeFormat = '%d.%m.%Y'
                start_day = datetime.datetime.strptime(datum[0], datetimeFormat)
                end_day = datetime.datetime.strptime(datum[1], datetimeFormat)
                
                for y in range(len(years)):
                    for m in range(len(month)):
                        ny = years[y]
                        nm = int(month[m])
                        new_days = monthrange(int(ny), int(nm))[1]+1
                        for d in range(1,new_days):
                            if len(str(d)) == 1:
                                d = "0"+str(d)
                            if len(str(m)) == 1:
                                m = "0"+str(m)
                            temp_day = str(d)+"."+str(nm)+"."+str(ny)
                            comp_day = datetime.datetime.strptime(temp_day, datetimeFormat)
                            if (comp_day > end_day) or (comp_day < start_day):
                                continue
                            else:
                                week_day = comp_day.isoweekday()
                                out_day = comp_day.strftime("%Y-%m-%d")
                                if spange == "1":
                                    if week_day == 3:
                                        new_datum.append(out_day)
                                elif spange == "2":
                                    if week_day == 6:
                                        new_datum.append(out_day)
                                elif spange == "3":
                                    if (week_day == 6) or (week_day == 3):
                                        new_datum.append(out_day)
            else:
                dat = datum[0].split(".")
                datum[0] = dat[2]+"-"+dat[1]+"-"+dat[0]
                new_datum.append(datum[0])
                                        
            out = ""
            for data in new_datum:
                try:
                    zahlen = Ziehung.objects.get(datum=data)
                    out += str(zahlen.datum)+"|"+str(zahlen.zahl1)+"|"+str(zahlen.zahl2)+"|"+str(zahlen.zahl3)+"|"+str(zahlen.zahl4)+"|"+str(zahlen.zahl5)+"|"+str(zahlen.zahl6)+"|"+str(zahlen.zahls)+"|"+str(zahlen.gk1)+"|"+str(zahlen.gk2)+"|"+str(zahlen.gk3)+"|"+str(zahlen.gk4)+"|"+str(zahlen.gk5)+"|"+str(zahlen.gk6)+"|"+str(zahlen.gk7)+"|"+str(zahlen.gk8)+"|"+str(zahlen.gk9)+"||"
                        
                except ObjectDoesNotExist:
                    out += str(data)+"|"+str("0")+"|"+str("0")+"|"+str("0")+"|"+str("0")+"|"+str("0")+"|"+str("0")+"|"+str("0")+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"|"+str("keine Ziehung am "+datum[-1])+"||"
                    
            return HttpResponse(out, content_type="text/html")
        
        else: 
            pass
    else:
        return HttpResponse("no", content_type="text/html")