from django.conf.urls import url
from django.contrib import admin
from . import views_dt
from .views_dt import zahltag,archiv

app_name = "de"

urlpatterns = [
    url(r'^$', views_dt.index, name="index"),
    url(r'^zahltag/(?P<pk>[-\d]+)/$', zahltag.as_view(), name="zahltag"),
    url(r'^archiv/$', archiv.as_view(), name="archiv"),
    url(r'^generator/$', views_dt.generator, name="generator"),
    url(r'^generator/neu/$', views_dt.parsen, name="neu"),
    url(r'^generator/get-datum/$', views_dt.getzahlen, name="checkdatum"),
]