from django.apps import AppConfig


class DeutschlandConfig(AppConfig):
    name = 'deutschland'
