
			$.extend($.gritter.options, {
		         class_name: 'gritter-light', 
		         position: 'top-right', 
		         fade_in_speed: 100, 
		         fade_out_speed: 100, 
		         time: 3000
	         });
			jQuery.fn.extend({
			    disable: function(state) {
			        return this.each(function() {
			            this.disabled = state;
			        });
			    }
			});
			function newInf (title,descr){
				$.gritter.add({
	                title: title,
	                text: descr,
	                sticky: false,
	                time: ''
	            });
			}
			$('#de,#eu').on('change', function(){
				var de = $('#de').val()
				var eu = $('#eu').val()
				if(de <= 0 || eu <= 0) {
					newInf('Falscher Wert!','Der Wert darf nicht kleiner als 1 sein!')
					$('#de').val(1)
					$('#eu').val(1)
					if(de <= 0) {
						$('#de-filter').html("Der Wert darf nicht kleiner als 1 sein!").attr('style','color:#cc0000');
					} else if(eu <= 0) {
						$('#eu-filter').html("Der Wert darf nicht kleiner als 1 sein!").attr('style','color:#cc0000');
					}
					$('input[type=submit]').disable(true);
				} else if(de > 12){
					newInf('Falscher Wert!','Der Wert darf nicht größer als 12 sein! Deutsche Lotterie erlaubt nur in seltenen Fällen mehr als 12 Felder.')
					$('#de').val(12)
					$('#de-filter').html("Der Wert darf nicht größer als 12 sein!").attr('style','color:#cc0000');
					$('input[type=submit]').disable(true);
				} else if(eu > 6){
					newInf('Falscher Wert!','Der Wert darf nicht größer als 6 sein! Europäische Lotterie erlaubt nur 6 Felder pro Spiel.')
					$('#eu').val(6)
					$('#eu-filter').html("Der Wert darf nicht größer als 6 sein!").attr('style','color:#cc0000');
					$('input[type=submit]').disable(true);
				} else {
					$('#de-filter,#eu-filter').html("Alles klar! Jetzt nur noch speichern!").attr('style','color:#27b039;');
					$('input[type=submit]').disable(false);
				}
			})
			$('#pw,#pw2').on('change', function(){
				var eins = $('#pw').val()
				var zwei = $('#pw2').val()
				if(eins != zwei){
					$('input[type=submit]').disable(true);
					$('#pw_stat').html("Passwörter müssen gleich sein!").attr('style',"color:#cc0000")
				} else {
					$('input[type=submit]').disable(false);
					$('#pw_stat').html("Passwörter sind gleich!").attr('style',"color:#27b039")
				}
			})
			$("#email").on('change', function(){
				$.ajax({
					type: "GET",
					url: "/mailcheck/",
					data: {
						"email": $("#email").val(),
					},
					dataType: "text",
					cache: false,
					success: function (data){
						if(data==="ok"){
							$('#echeck').html("Diese E-Mail ist frei. Sie können sich damit registrieren!").attr("style","color: rgba(0, 189, 104, 1);");
							$('#sub').disable(false);
						} else if(data==="no"){
							$('#echeck').html("Diese E-Mail ist besetzt! Wählen Sie sich eine andere oder melden Sie sich beim Admin!").attr("style","color: rgba(179, 0, 0, 1);");
							$('#sub').disable(true);
						} else if(data==="same"){
							$('#echeck').html("").attr("style","color: rgba(0, 189, 104, 1);");
							$('#sub').disable(false);
						}
					}
				});
			})
			var ziehcat = ["Nur am Mittwoch","Nur am Samstag", "am Mittwoch und am Samstag"]
			for(i=0;i<ziehcat.length;i++){
				var selected = "";
				var id = i+1;
				if(id == zieh) selected = " selected";
				var out = "<option value='"+id+"'"+selected+">"+ziehcat[i]+"</option>"
				$('#ziehung').append(out);
			}