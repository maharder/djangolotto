moment.locale('de');
var rc = 1
var feld = 1
var src = 0
var auszahlung = 0
var gebfelder = 0
var tickets = 0
for (r = 1; r < 8; r++) {
    var out = ""
    for (s = 1; s < 8; s++) {
        out += "<td>" + rc + "</td>"
        rc++
    }
    $('.table-gen').append("<tr>" + out + "</tr>")
}
for (r = 0; r < 1; r++) {
    var out = ""
    for (s = 0; s < 10; s++) {
        out += "<td>" + src + "</td>"
        src++
    }
    $('.table-super').append("<tr>" + out + "</tr>")
}
var ziehcat = ["Nur am Mittwoch","Nur am Samstag", "am Mittwoch und am Samstag"]
for(i=0;i<ziehcat.length;i++){
	var selected = "";
	var id = i+1;
	if(id == zieh) selected = " selected";
	var out = "<option value='"+id+"'"+selected+">"+ziehcat[i]+"</option>"
	$('#tagzieh').append(out);
}
var amZieh = $('#tagzieh').val()
$.extend($.gritter.options, {
    class_name: 'gritter-light',
    position: 'top-right',
    fade_in_speed: 100,
    fade_out_speed: 100,
    time: 3000
});
jQuery.fn.extend({
    disable: function(state) {
        return this.each(function() {
            this.disabled = state;
        });
    }
});
function newInf(title, descr) {
    $.gritter.add({
        title: title,
        text: descr,
        sticky: false,
        time: ''
    });
}
$('#add_list').disable(true)
$('#clear_list').disable(true)
function changeLen(list_temp_len) {
    var temp_length = 6 - list_temp_len
    $('#temp_len').html(temp_length)
    if (list_temp_len < 6) {
        $('#add_list').disable(true)
        $('#clear_list').disable(false)
    } else if (list_temp_len >= 1 && list_temp_len != 6) {
        $('#add_list').disable(true)
        $('#clear_list').disable(false)
    } else if (list_temp_len == 0) {
        $('#clear_list').disable(true)
        $('#add_list').disable(true)
    } else if (list_temp_len == 6) {
        $('#add_list').disable(false)
        $('#clear_list').disable(false)
    }
}
function tablereset() {
    $('#temp').val("")
    for (td = 0; td < $('.table-gen tr>td').length; td++) {
        $('.table-gen tr>td').removeClass('checked')
    }
}
function tableSreset() {
    for (td = 0; td < $('.table-super tr>td').length; td++) {
        $('.table-super tr>td').removeClass('checked')
    }
}
Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}
function removeA(arr) {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}
function butState(felder) {

    if (felder == 1) $('#del').disable(true)
    else $('#del').disable(false)
    if (felder == 12) $('#add').disable(true)
    else $('#add').disable(false)
}
function addField(){
    felder++
    $('#list').append("<li data-id=\"" + feld + "\" class=\"list-group-item\"><strong>Ihr Spielfeld " + (felder) + "</strong>: <span data-feld=\"" + feld + "\" class=\"spielzahlen\" id=\"feld-" + feld + "\"></span></li>")
    feld++
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function spanreset(){
	for(l = 1; l <= felder; l++) {
		$('#feld-' + l).html("")
	}
}
function compare(data, felder){
	$('#tage').html("")
	$('#tage_cont').html("")
	$('#ticket').html("")
	$('#gebuehren').html("")
	$('#gewinn').html("")
	auszahlung = 0
	gebfelder = 0
	tickets = 0
	for(i=0;i<data.length;i++){
		if(data[i] != ""){
			tickets++
			gebfelder++
			gleichen = []
			winZahlen = []
			var temp = data[i].split("|")
			var temp_date = temp[0].split("-")
			var temp_datum = temp_date[2]+"."+temp_date[1]+"."+temp_date[0]
			var active = ""
			if(i == 0) active = "active"
			else active = ""
			var html_tab = "<li role=\"presentation\" class=\""+active+"\"><a href=\"#date-"+temp[0]+"\" aria-controls=\"date-"+temp[0]+"\" role=\"tab\" data-toggle=\"tab\">Ziehung am "+temp_datum+"</a></li>";
			var html_cont = "<div role=\"tabpanel\" class=\"tab-pane "+active+"\" id=\"date-"+temp[0]+"\" role=\"tablist\">";
			html_cont += "<div class=\"row\">";
			html_cont += "<div class=\"col-md-4 center-block\">";
			html_cont += "<a class=\"btn btn-primary btn-block\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#date-"+temp[0]+"\" href=\"#gewinnklassen-"+i+"\" aria-controls=\"#gewinnklassen-"+i+"\">Gewinnklassen</a>";
			html_cont += "<a class=\"btn btn-meadow btn-block\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#date-"+temp[0]+"\" href=\"#gewinnzahlen-"+i+"\" aria-controls=\"#gewinnzahlen-"+i+"\">Gewinnzahlen</a>";
			html_cont += "</div>";
			html_cont += "<div class=\"col-md-8 vlists\">";
			html_cont += "<ul class=\"list-group collapse in\" id=\"gewinnklassen-"+i+"\" role=\"tabpanel\" aria-labelledby=\"gewinnklassen-"+i+"\">";
			for(g=1;g<10;g++){
				var k = g+7
				html_cont += "<li class=\"list-group-item\"><span class=\"badge\" id=\"gk"+g+"\">"+temp[k]+" &euro;</span><strong>Gewinnklasse #"+g+":</strong></li>"
			}
			html_cont += "</ul>";
			html_cont += "<ul class=\"list-group collapse\" id=\"gewinnzahlen-"+i+"\" role=\"tabpanel\" aria-labelledby=\"gewinnzahlen-"+i+"\">";
			html_cont += "<div class=\"wzahlen\">"
			for(g=1;g<7;g++){
				html_cont += "<div class=\"wzahl\">"+temp[g]+"</div>"
				winZahlen.push(temp[g])
			}
			html_cont += "<div class=\"wzahl super\">"+temp[7]+"</div>"
			html_cont += "</div>";
			for (var f=0;f<felder;f++){
				var tempList = []
				var ind = f+1
				var item = $('#feld-'+ind).text().split(", ")
				item.pop("")
				for(var it = 0; it < item.length; it++) {
					if(winZahlen.contains(item[it]))
						tempList.push(item[it])
				}
				gleichen.push(tempList)
			}
			for(li=0;li<gleichen.length;li++){
				var gk = 0
				var szaf = $('#feld-sz').text()
				var szp = 0
				if(szaf == temp[7]) szp = 1
				if(gleichen[li].length == 6){
					if(szp == 1) gk = 1
					else gk = 2
				} else if(gleichen[li].length == 5) {
					if(szp == 1) gk = 3
					else gk = 4
				} else if(gleichen[li].length == 4) {
					if(szp == 1) gk = 5
					else gk = 6
				} else if(gleichen[li].length == 3) {
					if(szp == 1) gk = 7
					else gk = 8
				} else if(gleichen[li].length == 2) {
					if(szp == 1) gk = 9
				}
				var gwa = ""
				var gwzahl = 0
				if(gk==1){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[8]+' &euro;)</span>'
					gwzahl = temp[8]
				} else if (gk==2){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[9]+' &euro;)</span>'
					gwzahl = temp[9]
				} else if (gk==3){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[10]+' &euro;)</span>'
					gwzahl = temp[10]
				} else if (gk==4){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[11]+' &euro;)</span>'
					gwzahl = temp[11]
				} else if (gk==5){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[12]+' &euro;)</span>'
					gwzahl = temp[12]
				} else if (gk==6){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[13]+' &euro;)</span>'
					gwzahl = temp[13]
				} else if (gk==7){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[14]+' &euro;)</span>'
					gwzahl = temp[14]
				} else if (gk==8){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[15]+' &euro;)</span>'
					gwzahl = temp[15]
				} else if (gk==9){
					gwa = '<span class="badge">Gewinnklasse '+gk+' ('+temp[16]+' &euro;)</span>'
					gwzahl = temp[16]
				}
				var sza = ""
				auszahlung = Number(parseFloat(parseFloat(auszahlung)+parseFloat(gwzahl))).toFixed(2)
				if(szaf == temp[7]){ 
					sza = ' <b style="color:#CC0000;border: 1px solid #CC0000;padding: 0px 5px;">'+temp[7]+'</b>'
				}
				html_cont += "<li class=\"list-group-item\">"+gwa+"<strong>Ergebnisse für Spielfeld #"+(li+1)+":</strong>&nbsp;&nbsp;&nbsp;"+gleichen[li]+sza+"</li>"
			}
			html_cont += "</ul>";
			html_cont += "</div>";
			html_cont += "</div>";
			$('#tage').append(html_tab)
			$('#tage_cont').append(html_cont)
		}
	}
	$('#ticket').html(Number(parseFloat(parseFloat(0.35)*parseFloat(tickets))).toFixed(2)+" €")
	$('#gebuehren').html(Number(parseFloat(1*parseFloat(felder)*parseFloat(tickets))).toFixed(2)+" €")
	$('#gewinn').html(auszahlung+ " €")
}
for (li = 0; li < felder; li++) {
    $('#list').append("<li data-id=\"" + feld + "\" class=\"list-group-item\"><strong>Ihr Spielfeld " + (li + 1) + "</strong>: <span data-feld=\"" + feld + "\" class=\"spielzahlen\" id=\"feld-" + feld + "\"></span></li>")
    feld++
}
$('#add').on('click', function() {
	addField()
    butState(felder)
})
$('#del').on('click', function() {
    felder--
    feld--
    $('#list>li:last').remove()
    butState(felder)
})
$('body').on('keypress', function(event) {
    if (event.keyCode == 43 && felder <= 11) { // Knopf +
    	addField()
        $('#add').disable(false)
    } else if (event.keyCode == 45 && felder >= 2) { // Knopf -
        felder--
        feld--
        $('#list>li:last').remove()
        $('#del').disable(false)
    } else if (event.keyCode == 120) { // Knopf X
        list_temp = []
        tablereset()
    } else if (event.keyCode == 97) { // Knopf A
        if (list_temp.length >= 6) {
            for (l = 1; l <= felder; l++) {
                var txtlen = $('#feld-' + l).text()
                if (txtlen.length == 0) {
                    var out_list = ""
                    list_temp.forEach(function(index) {
                        out_list += index + ", "
                    })
                    $('#feld-' + l).html(out_list)
                    break;
                } else {
                    continue;
                }
            }
            list_temp = []
            tablereset()
        }
    }
    butState(felder)
    changeLen(list_temp.length);
})
var list_temp = []
$('.table-gen tr>td').on('click', function() {
    if (list_temp.length != 6) {
        if (list_temp.contains($(this).text())) {
            $(this).removeClass('checked')
            removeA(list_temp, $(this).text())
        } else {
            $(this).addClass('checked')
            list_temp.push($(this).text())
        }
    } else {
        if (list_temp.contains($(this).text())) {
            $(this).removeClass('checked')
            removeA(list_temp, $(this).text())
        }
    }

    changeLen(list_temp.length);
    $('#temp').val(list_temp)
})
$('#clear_list').on('click', function() {
    list_temp = []
    tablereset()
    tableSreset()
    changeLen(list_temp.length);
})
$('#add_list').on('click', function() {
    if (list_temp.length >= 6) {
        for (l = 1; l <= felder; l++) {
            var txtlen = $('#feld-' + l).text()
            if (txtlen.length == 0) {
                var out_list = ""
                list_temp.forEach(function(index) {
                    out_list += index + ", "
                })
                $('#feld-' + l).html(out_list)
                break;
            } else {
                continue;
            }
        }
        list_temp = []
        tablereset()
        var actli = $('#list').find("li.active")
    	$(actli).removeClass('active')
    }
    changeLen(list_temp.length);
})
$('#rand').on('click', function(){
    list_temp = []
    tablereset()
    spanreset()
    tableSreset()
    var actli = $('ul').find("li.active")
	$(actli).removeClass('active')
	for(l = 1; l <= felder; l++) {
		for(z=0;z<6;z++){
	        var zahl = getRandomInt(1,49)
	        if(list_temp.contains(zahl)){
	        	z--
	        } else {
	        	list_temp.push(zahl)
	        }
		}
		var txtlen = $('#feld-' + l).text()
        if (txtlen.length == 0) {
            var out_list = ""
            list_temp.forEach(function(index) {
                out_list += index + ", "
            })
            $('#feld-' + l).html(out_list)
        } else {
            continue;
        }
		list_temp = []
	}
    $('#feld-sz').html(getRandomInt(0,9))
})
var chl = ""
$(document).on('click','.spielzahlen', function() {
	var actli = $('#list').find("li.active")
	if(actli.length > 0) {
		var spid = $(actli).data('id')
		var temsp = $('#list').find("span[data-feld='"+spid+"']")
		var out_list = ""
        list_temp.forEach(function(index) {
        	out_list += index + ", "
        })
        $(temsp).html(out_list)
		$(actli).removeClass('active')
	}
	
	var list = []
	tablereset()
	var item = $(this).text().split(', ')
	chl = $(this).data('feld')
	var temli = $('#list').find("li[data-id='"+chl+"']")
	$(temli).addClass('active')
	item.forEach(function(index) {
		if (index != "")
			list.push(index)
	})
	list_temp = list
	$(this).empty()
	$('#temp').val(list)
	var tds = $('.table-gen td')
	list.forEach(function(index) {
		var idt = index - 1
	    tds[idt].className = 'checked'
	})
    changeLen(list_temp.length)
})
$('#feld-sz').on('click', function() {
	var std = $('.table-super tr > td')
	var index = parseInt($(this).text())
	tableSreset()
	std[index].className = "checked"
})
$('.table-super tr > td').on('click', function(){
	var tdact = $('.table-super').find("td.checked")
	if(tdact.length >= 1) {
		tableSreset()
		$(this).addClass('checked')
		$('#feld-sz').html($(this).text())
	} else {
		$(this).addClass('checked')
		$('#feld-sz').html($(this).text())
	}
})
$('#new_zahlen').on('click', function(){
	$("#new_zahlen i").toggleClass("fa-bars").toggleClass("fa-refresh").toggleClass("fa-spin")
	$('#new_zahlen').toggleClass("btn-success").toggleClass("btn-default")
	$.ajax({
		type: "GET",
		url: "/de/generator/neu/",
		data: {
			"datum": $("#datum input").val(),
		},
		dataType: "text",
		cache: false,
		success: function (data){
			if(data==="ok"){
				newInf("Fertig!", "Die Seite ist jetzt auf dem aktuelsten Stand. Sie können weiter vergleichen!")
				$("#new_zahlen i").toggleClass("fa-bars").toggleClass("fa-refresh").toggleClass("fa-spin")
				$('#new_zahlen').toggleClass("btn-success").toggleClass("btn-default")
			} else {
				newInf("Hoppla!", "Ups! Irgendwas ist schief gelaufen!")
				$("#new_zahlen i").removeClass("fa-bars").removeClass("fa-refresh").removeClass("fa-spin").addClass("fa-times")
				$('#new_zahlen').removeClass("btn-success").removeClass("btn-default").addClass("btn-danger")
			}
		}
	});
})
var nowTemp = new Date();
var now = nowTemp.getDate() + "." + (nowTemp.getMonth() + 1) + "." + nowTemp.getFullYear()
var winZahlen = []
var disZahlen = []
if(amZieh == 1){
	disZahlen = [0,1,2,4,5,6]
} else if (amZieh == 2){
	disZahlen = [0,1,2,3,4,5,]
} else if (amZieh == 3){
	disZahlen = [0,1,2,4,5,]
}
$(document).on('change', '#tagzieh', function(){
	amZieh = $('#tagzieh').val()
	if(amZieh == 1){
		disZahlen = [0,1,2,4,5,6]
	} else if (amZieh == 2){
		disZahlen = [0,1,2,3,4,5,]
	} else if (amZieh == 3){
		disZahlen = [0,1,2,4,5,]
	}
	$('#dtdt').datepicker({
		onRenderCell: function (date, cellType) {
	        if (cellType == 'day') {
	            var day = date.getDay(),
	                isDisabled = disZahlen.indexOf(day) != -1;

	            return {
	                disabled: isDisabled
	            }
	        }
	    }
	})
})
var gleichen = []
$('#dtdt').datepicker({
	language: 'de',
	minDate: new Date(2013, 4, 4),
	maxDate: nowTemp,
	toggleSelected: false,
	onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
            var day = date.getDay(),
                isDisabled = disZahlen.indexOf(day) != -1;

            return {
                disabled: isDisabled
            }
        }
    },
    onSelect: function(fd, d, picker) {
    	if (!d) return;
    	
    	$.ajax({
			type: "GET",
			url: "/de/generator/get-datum/",
			data: {
				"datum": fd,
				"range": amZieh,
			},
			dataType: "text",
			cache: false,
			success: function (data){
				if(data!="ok"){
					$('#vergleichen').disable(false)
				} else {
					newInf('Hoppla!', 'Ein Fehler ist aufgetretten!')
				}
			}
		});
    }
})
$('#vergleichen').disable(true)
$('#vergleichen').on('click', function(event) {
	$.ajax({
		type: "GET",
		url: "/de/generator/get-datum/",
		data: {
			"datum": $('#dtdt').val(),
			"range": amZieh,
		},
		dataType: "text",
		cache: false,
		success: function (data){
			if(data!="ok"){
				var out = data.split("||")
				compare(out, felder)
	
				$('#vergleichen').disable(false)
			} else {
				newInf('Hoppla!', 'Ein Fehler ist aufgetretten!')
			}
		}
	});
	$('#vrgl').modal('toggle')
})
$(document).on('hover', 'ul[role="tabpanel"] li', function(){
	$('li').toggleClass('active')
})
$(function () {
  $('[data-toggle="tooltip"]').tooltip({
	  placement: 'auto',
	  selector: '',
	  container: '',
  })
})
$(document).on('click', 'a[data-toggle="collapse"]', function(){
	$('.vlists > ul').toggleClass('in')
})
$(document).on('change', '#dtdt', function(){
	var nowVal = $('#dtdt').val()
	if(nowVal != "") {
		$('#vergleichen').disable(false)
		$('#vrgllll').attr('title', "Vergleichen Sie Ihre Zahlen mit den Gewinnzahlen").data('original-title', "Vergleichen Sie Ihre Zahlen mit den Gewinnzahlen")
	} else {
		$('#vergleichen').disable(true)
		$('#vrgllll').attr('title', "Bitte, erst Datum wählen, dann vergleichen").data('original-title', "Bitte, erst Datum wählen, dann vergleichen")
	}
})
$(document).on('click', '.datepicker--cell-day', function(){
    var nowVal = $('#dtdt').val()
})
var Anleitung = new Tour({
	name: "Anleitung",
	steps: [
		{
			element: '.table-gen',
			title: 'Zahlenauswahl',
			content: 'Man wählt zuerst die Zahlen aus'
		},
		{
			element: '#temp',
			title: 'Zwischenspeicher',
			content: 'Hier sieht man die ausgewählten Zahlen'
		},
		{
			element: '#add_list',
			title: 'Auswählen',
			content: 'Hat man 6 Zahlen ausgewählt, so fügt man diese mit diesem Knopf dem Feld hinzu. Man kann auch den Tastaturtaste "A" dafür benutzen'
		},
		{
			element: '#clear_list',
			title: 'Löschen',
			content: 'Wenn man sich anders überlegt hat und andere Zahlen aussuchen möchte, so drückt man hier drauf. Oder auf die Taste "X"'
		},
		{
			element: '.table-super',
			title: 'Die Superzahl',
			content: 'Ist man mit der Auswahl von den 6 Zahlen fertig, so wählt man Diese aus. Reihenfolge ist egal. Zahl wird automatisch hinzugefügt',
			placement: 'bottom'
		},
		{
			element: '#meinewahl',
			title: 'Das Ausgewählte',
			content: 'Hier werden die gewählten Zahlen gelagert.',
			placement: 'left'
		},
		{
			element: '#add',
			title: 'Neues Spielfeld',
			content: 'Mit der Taste "+" oder diesem Knopf fügt man ein neues Spielfeld hinzu'
		},
		{
			element: '#del',
			title: 'Spielfeld löschen',
			content: 'Mit der Taste "-" oder diesem Knopf löscht man das letzte Feld',
			placement: 'left'
		},
		{
			element: '#rand',
			title: 'Zufallszahlen',
			content: 'Wenn man Beispielzahlen haben möchte, so drückt man hier drauf',
			onHidden: function (tour) {
				list_temp = []
			    tablereset()
			    spanreset()
			    tableSreset()
			    var actli = $('ul').find("li.active")
				$(actli).removeClass('active')
				for(l = 1; l <= felder; l++) {
					for(z=0;z<6;z++){
				        var zahl = getRandomInt(1,49)
				        if(list_temp.contains(zahl)){
				        	z--
				        } else {
				        	list_temp.push(zahl)
				        }
					}
					var txtlen = $('#feld-' + l).text()
			        if (txtlen.length == 0) {
			            var out_list = ""
			            list_temp.forEach(function(index) {
			                out_list += index + ", "
			            })
			            $('#feld-' + l).html(out_list)
			        } else {
			            continue;
			        }
					list_temp = []
				}
			    $('#feld-sz').html(getRandomInt(0,9))
			},
		},
		{
			element: '#feld-1',
			title: 'Zahlen ändern',
			content: 'Drauf klicken und in der Tabelle alte Zahlen mit neuen ersetzen'
		},
		{
			element: '#tagzieh',
			title: 'Verlosungstagauswahl',
			content: 'Wenn du nicht eingeloggt bist, so werden alle Lottoziehungen in betracht gezogen. Wenn du nur auf einige Wochentage dich konzentrieren möchtest - so wähle diese hier!'
		},
		{
			element: '#new_zahlen',
			title: 'Aktualisierung',
			content: 'Damit holst du die letzten Ziehungen und deren Gewinnklassen, damit man eigenen Zahlen mit diesen vergleichen kann',
			onHidden: function(tour){
				$("#new_zahlen i").toggleClass("fa-bars").toggleClass("fa-refresh").toggleClass("fa-spin")
				$('#new_zahlen').toggleClass("btn-success").toggleClass("btn-default")
				$.ajax({
					type: "GET",
					url: "/de/generator/neu/",
					data: {
						"datum": $("#datum input").val(),
					},
					dataType: "text",
					cache: false,
					success: function (data){
						if(data==="ok"){
							newInf("Fertig!", "Die Seite ist jetzt auf dem aktuelsten Stand. Sie können weiter vergleichen!")
							$("#new_zahlen i").toggleClass("fa-bars").toggleClass("fa-refresh").toggleClass("fa-spin")
							$('#new_zahlen').toggleClass("btn-success").toggleClass("btn-default")
						} else {
							newInf("Hoppla!", "Ups! Irgendwas ist schief gelaufen!")
							$("#new_zahlen i").removeClass("fa-bars").removeClass("fa-refresh").removeClass("fa-spin").addClass("fa-times")
							$('#new_zahlen').removeClass("btn-success").removeClass("btn-default").addClass("btn-danger")
						}
					}
				});
			}
		},
		{
			element: '#dtdt',
			title: 'Datum',
			content: 'Du kannst soviel wie du willst Tage auswählen - alle werden in der Vergleichsliste eingetragen. Dieser Punkt ist wichtig!',
			onHidden: function(tour){
				var myDatepicker = $('#dtdt').datepicker().data('datepicker');
				myDatepicker.show();
			}
		},
		{
			element: '.datepicker',
			title: 'Datum',
			content: 'Wähle Daten aus, die freigeschaltet sind.'
		},
		{
			element: '#vergleichen',
			title: 'Zahlen vergleichen',
			content: 'Wenn du Zahlen gewählt hast, sowie das Datum, so kannst du prüfen, ob du ein Sieger bist!',
			onHide: function(tour){
				$.ajax({
					type: "GET",
					url: "/de/generator/get-datum/",
					data: {
						"datum": $('#dtdt').val(),
						"range": amZieh,
					},
					dataType: "text",
					cache: false,
					success: function (data){
						if(data!="ok"){
							var out = data.split("||")
							compare(out, felder)
				
							$('#vergleichen').disable(false)
						} else {
							newInf('Hoppla!', 'Ein Fehler ist aufgetretten!')
						}
					}
				});
				
			},
			placement: 'left'
		},
		{
			element: '.statistik',
			title: 'Statistik',
			content: 'Hier wird Statistik angezeigt'
		},
		{
			element: '.tickets',
			title: 'Ticketpreis',
			content: 'Hier wird der Ticketpreis ermittelt und angezeigt'
		},
		{
			element: '#ticket',
			title: 'Ticketpreis',
			content: 'Für jedes Datum wird die Standartgebühr aufaddiert. 1,00€ x Datumspange',
			placement: 'top'
		},
		{
			element: '.felder',
			title: 'Felderpreis',
			content: 'Hier wird der Felderpreis ermittelt und angezeigt'
		},
		{
			element: '#gebuehren',
			title: 'Felderpreis',
			content: 'Die Summe wird aus jedem Spielfeld für jedes Datum ermittelt. 0,35€ x Spielfeld x Datumspange',
			placement: 'top'
		},
		{
			element: '.gewinn',
			title: 'Gewinn',
			content: 'Das gesamte Gewinn wird hier angezeigt'
		},
		{
			element: '#gewinn',
			title: 'Gewinn',
			content: 'Die Summe wird aus jeder Gewinnklasse zusammenaddiert.',
			placement: 'top'
		}
		
	],
	storage: false,
	backdrop: true,
	smartPlacement: true,
	template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation btn-group btn-group-justified' role='group'><div class='btn-group' role='group'><button class='btn btn-default btn-xs' data-role='prev'><i class='fa fa-chevron-left' aria-hidden='true'></i> Zurück</button></div><div class='btn-group' role='group'><button class='btn btn-default btn-xs' data-role='next'>Weiter <i class='fa fa-chevron-right' aria-hidden='true'></i></button></div><div class='btn-group' role='group'><button class='btn btn-default btn-xs' data-role='end'>Beenden <i class='fa fa-times-circle' aria-hidden='true'></i></button></div></div></div>",
})
Anleitung.init();
Anleitung.start();
$('#anleitung').on('click', function(){
	Anleitung.restart();
})