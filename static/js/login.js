jQuery.fn.extend({
    disable: function(state) {
        return this.each(function() {
            this.disabled = state;
        });
    }
});
$("#email").on('change', function(){
	$.ajax({
		type: "GET",
		url: "/mailcheck/",
		data: {
			"email": $("#email").val(),
		},
		dataType: "text",
		cache: false,
		success: function (data){
			if(data==="ok"){
				$('#check_email').html("Diese E-Mail ist frei. Sie können sich damit registrieren!").attr("style","color: rgba(0, 189, 104, 1);");
				$('#sub').disable(false);
			} else if(data==="no"){
				$('#check_email').html("Diese E-Mail ist besetzt! Wählen Sie sich eine andere oder melden Sie sich beim Admin!").attr("style","color: rgba(179, 0, 0, 1);");
				$('#sub').disable(true);
			}
		}
	});
})
$("#name").on('change', function(){
	$.ajax({
		type: "GET",
		url: "/namecheck/",
		data: {
			"name": $("#name").val(),
		},
		dataType: "text",
		cache: false,
		success: function (data){
			if(data==="ok"){
				$('#check_login').html("Dieser Benutzername ist frei. Sie können sich damit registrieren!").attr("style","color: rgba(0, 224, 123, 1);");
				$('#sub').disable(false);
			} else if(data==="no"){
				$('#check_login').html("Dieser Benutzername ist besetzt! Wählen Sie sich einen anderen oder melden Sie sich beim Admin!").attr("style","color: rgba(179, 0, 0, 1);");
				$('#sub').disable(true);
			}
		}
	});
})
$('#pw2').on('change', function(){
	var pw1 = $('#pw1').val()
	var pw2 = $('#pw2').val()
	
	if(pw1 != pw2){
		$('.check_pw').html("Passwörter müssen gleich sein!").attr("style","color: rgba(179, 0, 0, 1);");
		$('#sub').disable(true);
	} else {
		$('.check_pw').html("");
		$('#sub').disable(false);
	}
})